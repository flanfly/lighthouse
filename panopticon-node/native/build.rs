extern crate neon_build;

fn main() {
    neon_build::setup(); // must be called in build.rs

    println!("cargo:rustc-link-lib=dylib=stdc++");
    // add project-specific build logic here...
}
