#[macro_use]
extern crate neon;

extern crate crossbeam;
extern crate petgraph;
extern crate rand;
extern crate p8n_types as types;
extern crate p8n_analysis as analysis;
extern crate p8n_avr as avr;
extern crate p8n_amd64 as amd64;

use std::thread;
use std::sync::Arc;
use std::collections::HashMap;
use rand::{Rng, thread_rng};
use neon::prelude::*;
use crossbeam::queue::MsQueue;
use petgraph::{
    Direction,
    visit::EdgeRef,
};
use types::{
    Function, Uuid, CfgNode,
    Machine, Content, Constant, Variable,
    Guard,
};
use analysis::{
    Event, disassemble,
};

struct Line {
    offset: u64,
    address: u64,
    opcode: String,
    args: Vec<String>,
    // name: String,
    sources: Vec<u64>,
    targets: Vec<(u64,String)>,
}

enum Update {
    Todo{ name: String, uuid: Uuid },
    Function{ function: Function, version: f64, },
    Listing{ uuid: Uuid, source_ver: f64, lines: Vec<Line> },
    Done{ message: String, },
    Failure{ message: String, },
    Comment{ address: u64, comment: String, },
}

pub struct Panopticon {
    updates: Arc<MsQueue<Update>>,
    functions: HashMap<Uuid, (f64, Function)>,
    comments: HashMap<u64, String>,
    by_address: HashMap<u64, (Uuid, usize)>,
}

// Panopticon {
//  functions: [Function]
//
//  fn init()
//  fn applyUpdates()
//  fn fetchListing(uuid)
// }
//
// Function {
//  version: number,
//  name: String,
//  uuid: String,
//  entry: String,
//  listing: Async<[Line]>,
//  graph: Async<CtrlFlowGraph>,
// }
//
// Async<T> {
//  version: number,
//  state: String,
//  value: null / T,
// }
//
// Line {
//  version: number,
//  type: String,
//  address: String,
//
//  -- type == mnemonic --
//  opcode: String,
//  args: [String],
//  comment: String,
//
//  -- type == location --
//  name: null / String
//  sources:[String]
//
//  -- type == branch --
//  targets: [String]
// }
//
// CtrlFlowGraph {
//  ...
// }

fn build_async<'a>(cx: &mut CallContext<'a, JsPanopticon>) -> JsResult<'a, JsObject> {
    let ret = cx.empty_object();
    let version = cx.number(thread_rng().gen::<f64>());
    let state = cx.string("missing");

    ret.set(cx, "version", version)?;
    ret.set(cx, "state", state)?;
    Ok(ret)
}

fn build_function<'a>(cx: &mut CallContext<'a, JsPanopticon>, f: &Function) -> JsResult<'a, JsObject> {
    let meta = cx.empty_object();
    let uuid = cx.string(format!("{}", f.uuid()));
    let name = cx.string(f.name.clone());
    let entry = cx.number(f.entry_address() as f64);
    let version = cx.number(thread_rng().gen::<f64>());
    let listing = build_async(cx)?;

    meta.set(cx, "uuid", uuid)?;
    meta.set(cx, "name", name)?;
    meta.set(cx, "entry", entry)?;
    meta.set(cx, "version", version)?;
    meta.set(cx, "listing", listing)?;

    Ok(meta)
}

fn build_listing<'a>(cx: &mut CallContext<'a, JsPanopticon>, lines: Vec<Line>) -> JsResult<'a, JsArray> {
    let ret = cx.empty_array();
    let this = cx.this();

    for line in lines {
        let meta = cx.empty_object();
        let version = cx.number(thread_rng().gen::<f64>());
        let next_idx = cx.number(ret.len());
        let Line{
            offset, address, opcode, args: in_args,
            sources, targets
        } = line;

        let is_head = cx.boolean(offset == 0);
        let opcode = cx.string(opcode);
        let comment = cx.borrow(&this, |session| -> String {
            session.comments
                .get(&address).cloned().unwrap_or_default()
        });
        let comment = cx.string(comment);
        let offset = cx.string(format!("+ {}", offset));
        let address = cx.string(format!("{:#x}", address));
        let mut args = cx.empty_array();
        let mut srcs = cx.empty_array();
        let mut tgts = cx.empty_array();
        let mut gs = cx.empty_array();

        for (v,g) in targets {
            let next_idx = cx.number(tgts.len());

            let val = cx.string(format!("{}", v));
            let guard = cx.string(g);
            tgts.set(cx, next_idx, val)?;
            gs.set(cx, next_idx, guard)?;
        }

        for s in sources {
            let next_idx = cx.number(srcs.len());

            let val = cx.string(format!("{}", s));
            srcs.set(cx, next_idx, val)?;
        }

        for arg in in_args {
            let next_idx = cx.number(args.len());

            let val = cx.string(arg);
            args.set(cx, next_idx, val)?;
        }

        meta.set(cx, "isHead", is_head)?;
        meta.set(cx, "address", address)?;
        meta.set(cx, "offset", offset)?;
        meta.set(cx, "opcode", opcode)?;
        meta.set(cx, "args", args)?;
        meta.set(cx, "comment", comment)?;
        meta.set(cx, "sources", srcs)?;
        meta.set(cx, "targets", tgts)?;
        meta.set(cx, "guards", gs)?;
        meta.set(cx, "version", version)?;

        ret.set(cx, next_idx, meta)?;
    }

    Ok(ret)
}

fn spawn_disassembler(c: Content, queue: Arc<MsQueue<Update>>) {
    let entries = c.entry_points.iter().map(|p| {
        (p.offset, p.name.clone())
    }).collect::<Vec<_>>();
    let symbolic = c.symbolic.iter().map(|p| {
        (p.offset, p.name.clone())
    }).collect::<Vec<_>>();

    let callback = {
        let queue = queue.clone();

        Some(move |ev: Event| {
            match ev {
                Event::Update{ function } => {
                    queue.push(Update::Function{
                        function: function.clone(),
                        version: thread_rng().gen()
                    });
                }
                Event::Discover{ uuid, name,.. } =>
                    queue.push(Update::Todo{ uuid: uuid, name: name.to_string() }),
                ev => { println!("Unhandled event {:?}", ev); }
            }
        })
    };

    thread::spawn(move || {
        let th = {
            let queue = queue.clone();

            thread::spawn(move || {
                let res = match c.machine {
                    Machine::Avr => disassemble::<avr::Avr, _>(
                        avr::Mcu::atmega103(), &c.segments[0],
                        &entries, &symbolic, callback),
                    Machine::Amd64 => disassemble::<amd64::Amd64, _>(
                        amd64::Mode::Long, &c.segments[0],
                        &entries, &symbolic, callback),
                    Machine::Ia32 => disassemble::<amd64::Amd64, _>(
                        amd64::Mode::Protected, &c.segments[0],
                        &entries, &symbolic, callback),
                };

                match res {
                    Ok(_) => {
                        queue.push(Update::Done{ message: "Disassembly job finished".to_string() });
                    }
                    Err(e) => {
                        queue.push(Update::Failure{ message: format!("Disassembly job failed: {}", e) });
                    }
                }
            })
        };

        match th.join() {
            Ok(()) => {}
            Err(e) => {
                queue.push(Update::Failure{ message: format!("Disassembly job panicked: {:?}", e) });
            }
        }
    });
}

fn spawn_listinglizer(queue: Arc<MsQueue<Update>>, f: Function, version: f64) {
    thread::spawn(move || {
        let th = {
            let queue = queue.clone();
            thread::spawn(move || {
                let mut lines = Vec::default();
                let mut bbs = f.basic_blocks().collect::<Vec<_>>();
                let mut by_addr = HashMap::new();

                bbs.sort_by_key(|x| x.1.area.start);

                let mut idx = 0;
                for &(bb_idx, _) in bbs.iter() {
                    for (_, mne) in f.mnemonics(bb_idx) {
                        let opcode = f.strings.value(mne.opcode).unwrap();

                        if !opcode.starts_with("__") {
                            by_addr.insert(mne.area.start, idx);
                            idx += 1;
                        }
                    }
                }

                for (bb_idx, bb) in bbs {
                    let cfg = f.cflow_graph();

                    // incoming jumps
                    let sources = cfg
                        .edges_directed(bb.node, Direction::Incoming)
                        .filter_map(|e| match cfg.node_weight(e.source()) {
                            Some(&CfgNode::BasicBlock(other_idx)) => {
                                let other_bb = f.basic_block(other_idx);
                                if bb.area.start != other_bb.area.end {
                                    by_addr.get(&other_bb.area.start).map(|&x| x as u64)
                                } else {
                                    None
                                }
                            }
                            _ => None,
                        }).collect::<Vec<_>>();

                    // outgoing jumps
                    let targets = cfg
                        .edges_directed(bb.node, Direction::Outgoing)
                        .filter_map(|e| match cfg.node_weight(e.target()) {
                            Some(&CfgNode::BasicBlock(other_idx)) => {
                                let other_bb = f.basic_block(other_idx);
                                if bb.area.end != other_bb.area.start {
                                    let g = match e.weight() {
                                        Guard::True => "".to_string(),
                                        Guard::False => "never".to_string(),
                                        Guard::Predicate{ flag: Variable{ name,.. }, expected: true } => {
                                            format!("if {}", f.names.value(*name).unwrap().base())
                                        }
                                        Guard::Predicate{ flag: Variable{ name,.. }, expected: false } => {
                                            format!("if !{}", f.names.value(*name).unwrap().base())
                                        }
                                    };
                                    by_addr.get(&other_bb.area.start).map(|&x| (x as u64,g))
                                } else {
                                    None
                                }
                            }
                            _ => None,
                        }).collect::<Vec<_>>();

                    let mne_cnt = f.mnemonics(bb_idx).filter(|(_, mne)| {
                        !f.strings.value(mne.opcode).unwrap().starts_with("__")
                    }).count();
                    let iter = f.mnemonics(bb_idx).filter(|(_, mne)| {
                        !f.strings.value(mne.opcode).unwrap().starts_with("__")
                    });

                    // mnemonics
                    for (pos, (_, mne)) in iter.enumerate() {
                        let opcode = f.strings.value(mne.opcode).unwrap();

                        let mut args = Vec::with_capacity(mne.operands.len());

                        for val in mne.operands.iter() {
                            match val {
                                &types::Value::Constant(Constant{ value,.. }) => {
                                    args.push(format!("{:#x}", value));
                                }
                                &types::Value::Variable(Variable{ name,.. }) => {
                                    args.push(f.names.value(name).unwrap().base().to_string());
                                }
                                &types::Value::Undefined => {
                                    args.push("?".to_string());
                                }
                            }
                        }

                        //assert_eq!(pos, by_addr[&mne.area.start]);

                        let off = if mne.area.start >= bb.area.start {
                            mne.area.start - bb.area.start
                        } else {
                            mne.area.start
                        };
                        lines.push(Line{
                            offset: off,
                            address: mne.area.start,
                            opcode: opcode.to_string(),
                            args: args,
                            sources: if pos == 0 { sources.clone() } else { vec![] },
                            targets: if pos == mne_cnt - 1 { targets.clone() } else { vec![] },
                        });
                    }
                }

                queue.push(Update::Listing{
                    uuid: f.uuid().clone(),
                    source_ver: version,
                    lines: lines });
            })
        };

        match th.join() {
            Ok(()) => {}
            Err(e) => {
                queue.push(Update::Failure{ message: format!("Listinglizer job panicked: {:?}", e) });
            }
        }
    });
}

declare_types! {
    pub class JsPanopticon for Panopticon {
        init(mut cx) {
            use std::path::Path;

            let path = cx.argument::<JsString>(0)?.value();
            println!("open({})",path);

            let res = Content::load(&Path::new(&path)).and_then(|c| {
                let ret = Panopticon{
                    updates: Arc::new(MsQueue::default()),
                    functions: HashMap::default(),
                    by_address: HashMap::default(),
                    comments: HashMap::default(),
                };

                spawn_disassembler(c, ret.updates.clone());

                Ok(ret)
            });

            match res {
                Ok(x) => Ok(x),
                Err(s) => cx.throw_error(format!("{}", s)),
            }
        }

        method applyUpdates(mut cx) {
            let mut this = cx.this();
            let updates_waiting = cx.borrow(&this, |session| {
                !session.updates.is_empty()
            });

            if updates_waiting {
                while let Some(up) = cx.borrow_mut(&mut this, |s| s.updates.try_pop()) {
                    match up {
                        Update::Function{ function: f, version } => {
                            let funcs = match this.get(&mut cx, "functions")?.downcast::<JsObject>() {
                                Ok(fns) => fns,
                                Err(_) => {
                                    let fns = cx.empty_object();
                                    this.set(&mut cx, "functions", fns)?;
                                    fns
                                }
                            };
                            let uuid = cx.string(format!("{}", f.uuid()));
                            let meta = build_function(&mut cx, &f)?;

                            cx.borrow_mut(&mut this, |mut s| s.functions.insert(f.uuid().clone(), (version, f)));
                            funcs.set(&mut cx, uuid, meta)?;
                        }
                        Update::Todo{ name, uuid } => {
                            let funcs = match this.get(&mut cx, "functions")?.downcast::<JsObject>() {
                                Ok(fns) => fns,
                                Err(_) => {
                                    let fns = cx.empty_object();
                                    this.set(&mut cx, "functions", fns)?;
                                    fns
                                }
                            };
                            let meta = cx.empty_object();
                            let uuid = cx.string(format!("{}", uuid));
                            let name = cx.string(name);

                            meta.set(&mut cx, "uuid", uuid)?;
                            meta.set(&mut cx, "name", name)?;

                            funcs.set(&mut cx, uuid, meta)?;
                        }
                        Update::Listing{ uuid, source_ver, lines } => {
                            let uuid = cx.string(format!("{}", uuid));
                            let funcs = match this.get(&mut cx, "functions")?.downcast::<JsObject>() {
                                Ok(fns) => fns,
                                Err(_) => {
                                    let fns = cx.empty_object();
                                    this.set(&mut cx, "functions", fns)?;
                                    fns
                                }
                            };
                            let listing = build_listing(&mut cx, lines)?;
                            let func = funcs.get(&mut cx, uuid)?.downcast_or_throw::<JsObject,_>(&mut cx)?;
                            // XXX: check source_ver == func.version, otherwise restart
                            let async = cx.empty_object();
                            let version = cx.number(thread_rng().gen::<f64>());
                            let state = cx.string("done");

                            async.set(&mut cx, "version", version)?;
                            async.set(&mut cx, "state", state)?;
                            async.set(&mut cx, "value", listing)?;
                            func.set(&mut cx, "listing", async)?;
                        }
                        Update::Comment{ address, comment } => {
                            cx.borrow_mut(&mut this, |mut s| {
                                s.comments.insert(address, comment);
                            });
                        }
                        Update::Done{ message } | Update::Failure{ message } => { eprintln!("====== {}", message); }
                    }
                }

                Ok(cx.boolean(true).upcast())
            } else {
                Ok(cx.boolean(false).upcast())
            }
        }

        method fetchListing(mut cx) {
            use std::str::FromStr;

            let uuid_str = cx.argument::<JsString>(0)?;
            let uuid = match Uuid::from_str(&uuid_str.value()) {
                Ok(uu) => uu,
                Err(()) => { return cx.throw_error("argument is not a valid UUID"); }
            };
            let this = cx.this();
            let opt = cx.borrow(&this, |s| {
                s.functions.get(&uuid).cloned().map(|(ver, func)| {
                    (func, ver, s.updates.clone())
                })
            });
            let (func, ver, queue) = match opt {
                Some((func, ver, queue)) => (func, ver, queue),
                None => { return cx.throw_error("argument is not a valid UUID"); }
            };
            spawn_listinglizer(queue, func, ver);
            let funcs = match this.get(&mut cx, "functions")?.downcast::<JsObject>() {
                Ok(fns) => fns,
                Err(_) => {
                    let fns = cx.empty_object();
                    this.set(&mut cx, "functions", fns)?;
                    fns
                }
            };
            let func = funcs.get(&mut cx, uuid_str)?.downcast_or_throw::<JsObject,_>(&mut cx)?;
            let async = cx.empty_object();
            let version = cx.number(thread_rng().gen::<f64>());
            let state = cx.string("running");

            async.set(&mut cx, "version", version)?;
            async.set(&mut cx, "state", state)?;
            func.set(&mut cx, "listing", async)?;

            Ok(cx.undefined().as_value(&mut cx))
        }

        method comment(mut cx) {
            use std::str::FromStr;

            let addr = match u64::from_str(&cx.argument::<JsString>(0)?.value()) {
                Ok(addr) => addr,
                Err(e) => {
                    return cx.throw_error(format!("argument is not a valid address: {}", e));
                }
            };

            let cmnt = cx.argument::<JsString>(1)?.value();
            let this = cx.this();

            cx.borrow(&this, |s| {
                s.updates.push(Update::Comment{ comment: cmnt, address: addr });
            });

            Ok(cx.undefined().as_value(&mut cx))
        }
    }
}

register_module!(mut cx, {
    cx.export_class::<JsPanopticon>("Panopticon")?;

    Ok(())
});
