window.Panopticon = require('panopticon-node');
window.argument = "";
window.isDev = require('electron-is-dev');

const panopArg = "--panopticon=";

for (let x of require('electron').remote.process.argv) {
  if (x.startsWith(panopArg)) {
    window.argument = x.slice(panopArg.length);
    break;
  }
}
