import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Sidebar from './Sidebar';
import Loader from './Loader';

import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      backend: new window.Panopticon(props.file),
    };
    this.fetchBackendState = this.fetchBackendState.bind(this);
  }

  componentDidMount() {
    setTimeout(
      () => this.fetchBackendState(),
      200
    );
  }

  fetchBackendState() {
    if (this.state.backend.applyUpdates()) {
      this.setState({
        backend: this.state.backend,
      });
    }

    setTimeout(
      () => this.fetchBackendState(),
      200
    );
  }

  render() {
    const backend = this.state.backend;

    if (backend && backend.functions) {
      const functs = backend.functions;

      return (
        <Router>
          <div className="App">
            <Switch>
              <Route exact path="/" render={props =>
                <Sidebar
                  functions={functs}
                  selectedFunc={null}
                  className="App-panel"
                />
              } />

              <Route
                exact path="/:func/:row?"
                render={props => {
                  const uuid = props.match.params["func"];
                  let row = props.match.params["row"];
                  const func = functs[uuid];

                  if (row === "" || row === undefined) {
                    // XXX: make this the entry
                    row = 0;
                  } else {
                    // XXX: more schemes
                    row = parseInt(row, 10);
                  }

                  if (func) {
                    return [
                      <Sidebar
                        functions={functs}
                        selectedFunc={uuid}
                        className="App-panel"
                      />,
                      <Loader
                        scrollToRow={row}
                        func={func}
                        backend={backend}
                        className="App-panel"
                        fullLineHeight={25}
                        halfLineHeight={13}
                        addressColumnWidth={70 + 75}
                      />
                    ];
                  } else {
                    return [
                      <Sidebar
                        functions={functs}
                        selectedFunc={uuid}
                        className="App-panel"
                      />,
                      <div>
                        Unknown function {props.location.pathname}
                      </div>
                    ];
                  }
                }}
              />

              <Route render={props => <div>Unknown URL</div>} />
            </Switch>
          </div>
        </Router>
      );
    } else {
      return (<div>Backend not ready</div>);
    }
  }
}

export default App;
