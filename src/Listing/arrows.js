import React from 'react';

function Arrows({ width, height, arrows, halfLineHeight, fullLineHeight, trackSpacing, firstTrack }) {
  let paths = []

  for (let i in arrows) {
    const arrow = arrows[i];

    if (arrow.track !== undefined) {
      const locationAdjust = fullLineHeight - (halfLineHeight / 2) - 2;
      const branchAdjust = halfLineHeight / 2;
      const color = "#333333";
      const w = parseInt(width, 10) - 70;
      const fw = parseInt(width, 10) - 70;
      const branch = arrow.branch;
      const location = arrow.location;
      const track = w - (arrow.track * trackSpacing + firstTrack);
      const incomplete_len = 35;
      let d1 = "";
      let isBackedge = false;

      if (branch.type === 'complete' && location.type === 'complete') {
        const from_y = branch.y + branchAdjust;
        const to_y = location.y + locationAdjust;

        isBackedge = from_y > to_y;
        d1 =
          `M ${fw} ${from_y} ` +
          `L ${track} ${from_y} ` +
          `L ${track} ${to_y}  ` +
          `L ${w - 10} ${to_y}`;
      } else if (branch.type === 'complete' && location.type === 'incomplete') {
        const y = branch.y + branchAdjust;

        isBackedge = location.direction === 'up';
        d1 =
          `M ${fw} ${y} ` +
          `L ${track} ${y} ` +
          `l 0 ${location.direction === 'up' ? -1 * incomplete_len : incomplete_len}`;
      } else if (branch.type === 'incomplete' && location.type === 'complete') {
        const y = location.y + locationAdjust;

        isBackedge = branch.direction === 'down';
        d1 =
          `M ${track} ${(branch.direction === 'up' ? -1 * incomplete_len : incomplete_len) + y}` +
          `L ${track} ${y}  ` +
          `L ${w - 10} ${y}`;
      }

      if (d1 !== "") {
        paths.push((
          <path
            key={"arrow-" + i}
            d={d1}
            fill='none'
            strokeLinejoin='round'
            stroke={color}
            strokeWidth='1'
            strokeDasharray={isBackedge ? "5,2" : "0"}
          />
        ));

        if (location.type === 'complete') {
          const d2 =
            `M ${w - 10} ${location.y + locationAdjust - 4}` +
            `l 0 8 l 10 -4 l -10 -4`;

          paths.push((
            <path
              d={d2}
              key={"head-" + i}
              fill={color}
              strokeLinejoin='round'
              stroke={color}
              strokeWidth='0'
            />
          ));
        }
      }
    }
  }

  return (
    <svg height={height} width={width}>
      {paths}
    </svg>
  );
}

export default Arrows;
