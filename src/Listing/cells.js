import React from 'react';
import { CellMeasurer, CellMeasurerCache } from 'react-virtualized';
import { EditableText } from '@blueprintjs/core';

import Mnemonic from '../Mnemonic.js';

const measurerCache = new CellMeasurerCache({
  defaultWidth: 100,
  minWidth: 0,
  fixedHeight: true,
});

function getColumnWidth({ index }) {
  switch (index) {
    case 0:
      return 70 + 75;
    case 1:
      return measurerCache.columnWidth({ index: index });
    case 2:
      return measurerCache.columnWidth({ index: index });
    default:
      console.error("Unknown listing column " + index.toString());
      return 0;
  }
}

function cellRenderer({ columnIndex, key, rowIndex, style, parent }, listing, uuid) {
  const line = listing[rowIndex];
  const myStyle = Object.assign({whiteSpace: "nowrap"}, style);

  let child;

  switch (columnIndex) {
    // Address
    case 0:
      child = (
        <Address
          key={key}
          style={myStyle}
          address={line.address}
          offset={line.offset}
          isHead={line.isHead}
          from={line.sources && line.sources.length > 0 ? line.sources[0] : ""}
          to={line.targets && line.targets.length > 0 ? line.targets[0] : ""}
        />
      );
      break;

    // Mnenonic
    case 1:
      child = (
        <Mnemonic
          key={key}
          style={myStyle}
          mnemonic={line}
          uuid={uuid}
          guard={line.guards && line.guards.length > 0 ? line.guards[0] : ""}
        />
      );
      break;

    // Comment
    case 2:
      child = (
        <Comment
          key={key}
          style={myStyle}
          comment={line.comment}
        />
      );
      break;

    default:
      console.error("Unknown column index", columnIndex);
  }

  return (
    <CellMeasurer
      cache={measurerCache}
      columnIndex={columnIndex}
      key={key}
      parent={parent}
      rowIndex={rowIndex}
    >
      {child}
    </CellMeasurer>
  );
}

function Address({ address, offset, style, from, to, isHead }) {
  const props = {
    'className': "Address",
    'data-shortend-address': (isHead ? address : offset),
    'data-full-address': address,
  };

  return (
    <div style={style} className="Listing-column Address-background">
      <div {...props} />
    </div>
  );
}

function Comment({ comment, style }) {
  return (
    <div style={style} className="Listing-column">
      <div className="Comment">
        <EditableText defaultValue={comment} placeholder="Click here to comment" confirmOnEnterKey={true} />
      </div>
    </div>
  );
}

export { cellRenderer, getColumnWidth, measurerCache };
