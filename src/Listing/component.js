import React from 'react';
import { Grid, AutoSizer } from 'react-virtualized';

import { cellRangeRenderer } from './renderer.js';
import { measurerCache, getColumnWidth, cellRenderer } from './cells.js';

import './style.css';

class Listing extends React.Component {
  constructor(props) {
    super(props);

    // arrow-id -> arrow
    this.activeArrows = {};
    this.state = {
      startAddress: null,
      startRow: 0,
    }
  }

  render() {
    const {
      func, listing, scrollToRow, className, addressColumnWidth,
      fullLineHeight, halfLineHeight
    } = this.props;

    return (
      <div className={[className, "Listing"].join(" ")} style={{display: "flex", alignItems: "stretch"}}>
        <div className={"Listing-block-list"} style={{flex: 1}}>
          <style>{`
            :root {
              --full-line-height: ${fullLineHeight}px;
              --half-line-height: ${halfLineHeight}px;
              --address-column-width: ${addressColumnWidth}px;
              }
            `}
          </style>
          <AutoSizer>
            {({ height, width }) => (
              <Grid
                scrollToAlignment="center"
                scrollToRow={scrollToRow >= 0 && scrollToRow < listing.length ? scrollToRow : 0}
                columnWidth={(props) => {
                  if (props.index === 0) {
                    return addressColumnWidth;
                  } else {
                    return getColumnWidth(props);
                  }
                }}
                width={width}
                cellRenderer={(x) => cellRenderer(x, listing, func.uuid)}
                columnCount={3}
                height={height}
                rowCount={listing.length}
                rowHeight={({ index }) => {
                  if (listing[index].isHead) {
                    return fullLineHeight;
                  } else {
                    return halfLineHeight;
                  }
                }}
                deferredMeasurementCache={measurerCache}
                cellRangeRenderer={
                  (x) => cellRangeRenderer(x, this.activeArrows,
                    halfLineHeight, fullLineHeight
                  )}
              />
            )}
          </AutoSizer>
        </div>
      </div>
    );
  }
}
export default Listing;
