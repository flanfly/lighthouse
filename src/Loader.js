import React from 'react';
import Listing from './Listing';

class Loader extends React.Component {
  render() {
    const { func, className } = this.props;

    if (func && func.listing && func.listing.state === "done") {
      return (<Listing {...this.props} listing={func.listing.value} />);
    } else {
      return (
        <div className={[className,"Listing","Listing-placeholder"].join(" ")}>
          <div>No active function</div>
        </div>
      );
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { func, backend } = this.props;

    if (backend &&
        func && func.listing && func.listing.state === "missing")
    {
      backend.fetchListing(func.uuid);
    }
  }
}
export default Loader;
