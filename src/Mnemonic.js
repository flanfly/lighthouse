import React from 'react';
import { Link } from "react-router-dom";
import ReactHoverObserver from 'react-hover-observer';

import './Mnemonic.css';

const NUMBER_RE = /^(((0x)?[a-fA-F0-9]+)|([0-9]+))$/
const REGISTER_RE = /^((E|R)?(A|B|C|D)X)|((E|R)?(DI|SI|IP|SP|BP)|(R(8|9|10|11|12|13|14|15)(D|W|B)?))$/

function Mnemonic({ mnemonic, style, uuid }) {
  const args = mnemonic.args.map((arg, idx) => {
    if (NUMBER_RE.test(arg)) {
      return <span key={idx} className="Mnemonic-arg-numeric">{arg}</span>;
    } else if (REGISTER_RE.test(arg)) {
      return <span key={idx} className="Mnemonic-arg-register">{arg}</span>;
    } else {
      return <span key={idx} className="Mnemonic-arg">{arg}</span>;
    }
  }).reduce((prev, curr) => prev.length === 0 ? [curr] : [prev, ', ', curr], []);

  if (mnemonic.targets && mnemonic.targets.length > 0) {
    return (
      <div className="Mnemonic Listing-column" style={style}>
        <Link to={"/" + uuid.toString() + "/" + mnemonic.targets[0]} className="Mnemonic-jump">
          <ReactHoverObserver>
            <span className="Mnemonic-opcode">{mnemonic.opcode}</span>
            <span className="Mnemonic-args">&nbsp;{args}</span>
          </ReactHoverObserver>
        </Link>
      </div>
    );
  } else {
    return (
      <div className="Mnemonic Listing-column" style={style}>
        <div>
          <span className="Mnemonic-opcode">{mnemonic.opcode}</span>
          <span className="Mnemonic-args">&nbsp;{args}</span>
        </div>
      </div>
    );
  }
}

export default Mnemonic;
