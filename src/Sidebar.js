import React from 'react';
import { Link } from "react-router-dom";
import { List, AutoSizer } from 'react-virtualized';

import './Sidebar.css';

class Item extends React.Component {
   render() {
    let cls = this.props.func.entry ? "Sidebar-Item" : "Sidebar-ItemDisabled";

    if (this.props.selectedFunc === this.props.func.uuid) {
      cls += " SelectedFunction"
    }

    return (
      <Link to={"/" + this.props.func.uuid.toString()} className={cls}>
        {this.props.func.name}
      </Link>);
  }
}


class Sidebar extends React.PureComponent {
  rowRenderer({ key, index, style }, uuids, functions, selectedFunc) {
    return (
      <div key={key} style={style}>
        <Item selectedFunc={selectedFunc} func={functions[uuids[index]]} />
      </div>
    )
  }

  render() {
    const { className, functions, selectedFunc } = this.props;
    const uuids = Object.keys(functions);

    return (
      <div className={[className,"Sidebar"].join(" ")}>
        <AutoSizer>
        {({ height, width }) => (
          <List
            height={height}
            width={width}
            rowCount={uuids.length}
            rowRenderer={(x) => this.rowRenderer(x, uuids, functions, selectedFunc)}
            rowHeight={28}
          />
        )}
        </AutoSizer>
      </div>);
  }
}

export default Sidebar;
