import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.js';
import registerServiceWorker from './registerServiceWorker';

import './index.css';
import 'normalize.css/normalize.css';
//import '@blueprintjs/icons/lib/css/blueprint-icons.css';
import '@blueprintjs/core/lib/css/blueprint.css';

ReactDOM.render(
  <App file={window.argument} />,
  document.getElementById('root')
);

if (window.isDev) {
  registerServiceWorker();
}
